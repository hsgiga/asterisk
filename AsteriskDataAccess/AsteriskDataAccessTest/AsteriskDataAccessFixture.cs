﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AsteriskDataAccess;
using NUnit.Framework;

namespace AsteriskDataAccessTest
{
    [TestFixture]
    public class AsteriskDataAccessFixture
    {

        private asteriskEntitiesConnections _asteriskEntities;

        [TestFixtureSetUp]
        public void AsteriskDataAccessUp()
        {
            _asteriskEntities = new asteriskEntitiesConnections();
        }

        [TestFixtureTearDown]
        public void AsteriskDataAccessDown()
        {
            _asteriskEntities.Dispose();
        }


        [Test]
        public void TestFriend()
        {
            _asteriskEntities = new asteriskEntitiesConnections();

            var friend = new Friend(
                name:"vbondarenko",
                username: "vbondarenko",
                secret:"123");
            _asteriskEntities.sipusers.Add(friend);
            _asteriskEntities.SaveChanges();
        }

        [Test]
        public void TestPeer()
        {
            _asteriskEntities = new asteriskEntitiesConnections();

            var peer = new Peer(
                name: "multifon-inbound",
                username: "",
                secret: "",
                host: "",
                fromdomain: "",
                fromuser: "",
                port: 506);
        }

        [Test]
        public void TestExtension()
        {
            _asteriskEntities = new asteriskEntitiesConnections();

            var extension = new extension(context: "phones", exten: "303", priority: 1, app: "Dial", appdata: "");

            _asteriskEntities.extensions.Add(extension);
            _asteriskEntities.extensions.Add(new extension(context: "forward", exten: "pyatov", priority: 1, app: "Dial",
                appdata: ""));

            _asteriskEntities.SaveChanges();
        }
    }
}
